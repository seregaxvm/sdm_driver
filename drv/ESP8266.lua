local drv = sdm.driver_add("ESP8266")

sdm.method_add(drv, "_poll", nil,
               function(dev, drv, par)
                  return (sdm.device_basename(dev) == "ESP8266") and (par == nil)
               end
)

sdm.method_add(drv, "_init", nil,
               function(dev, drv, par)
                  local attr = sdm.attr_handle(dev, "id")
                  sdm.attr_set(attr, node.chipid())
                  attr = sdm.attr_handle(dev, "float")
                  sdm.attr_set(attr, 3 / 2 ~= 1)
               end
)

sdm.attr_add(drv, "id", "Chip ID", 0,
             function(drv)
                local attr = sdm.attr_drv_handle(drv, "id")
                return sdm.attr_data(attr)
             end,
             nil
)

sdm.attr_add(drv, "float", "Floating point build", false,
             function(drv)
                local attr = sdm.attr_drv_handle(drv, "float")
                return sdm.attr_data(attr)
             end,
             nil
)

sdm.method_add(drv, "heap", "Free heap", function() return node.heap() end)

drv = nil
