local drv = sdm.driver_add("MCP3208")

sdm.method_add(
   drv, "_poll", nil,
   function(dev, drv, par)
      return sdm.device_basename(dev) == "MCP3208"
   end
)

sdm.method_add(drv, "_init", nil,
               function(dev, drv, par)
                  local setup = sdm.method_func(sdm.method_dev_handle(par, "setup"))
                  setup(par, 19, 10000)
                  sdm.attr_copy(dev, "ref")
               end
)

sdm.method_add(drv, "single", "Single ended measure 0|1|2|3|4|5|6|7",
               function(dev, channel)
                  local par = sdm.device_parent(dev)
                  local ex = sdm.method_func(sdm.method_dev_handle(par, "exchange"))
                  local data = bit.bor(24, channel)
                  data = bit.lshift(data, 14)
                  local rv = ex(par, dev, {data})
                  rv = bit.band(rv[1], 4095)
                  local ref = sdm.attr_data(sdm.attr_handle(dev, "ref"))
                  if ref ~= nil then
                     rv = ref * rv / 4096
                  end
                  return rv
               end
)

sdm.method_add(drv, "differential", "Differential measure 0|2|4|6",
               function(dev, channel)
                  local par = sdm.device_parent(dev)
                  local ex = sdm.method_func(sdm.method_dev_handle(par, "exchange"))
                  local data = bit.bor(16, channel)
                  data = bit.lshift(data, 14)
                  local rv = ex(par, dev, {data})
                  rv = bit.band(rv[1], 4095)
                  local ref = sdm.attr_data(sdm.attr_handle(dev, "ref"))
                  if ref ~= nil then
                     rv = ref * rv / 4096
                  end
                  return rv
               end
)

if 3/2~=1 then
   sdm.attr_add(drv, "ref", "Reference voltage", 5,
                function(dev)
                   return sdm.attr_data(sdm.attr_handle(dev, "ref"))
                end,
                function(dev, value)
                   sdm.attr_set(sdm.attr_handle(dev, "ref"), value)
                end
   )
end

drv = nil
