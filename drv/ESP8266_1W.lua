local drv = sdm.driver_add("ESP8266_1W")

local pins = {}

sdm.method_add(drv, "_poll", nil, function(dev, drv, par) return (sdm.device_name(dev) == "ESP8266_1W") and (sdm.device_name(par) == "ESP8266") end)

sdm.method_add(drv, "_free", nil,
               function(dev, drv, par)
                  for pin, _ in pairs(pins) do
                     sdm.pin_free(pin)
                  end
               end
)

sdm.method_add(
   drv, "setup", "Setup pin",
   function(bus, dev)
      local pin = sdm.attr_data(sdm.local_attr_handle(dev, "datapin"))
      if pin ~= nil then
         if sdm.pin_request(pin) then
            if pins[pin] == nil then
               pins[pin] = 1
            else
               pins[pin] = pins[pin] + 1
            end
            ow.setup(pin)
         end
      end
   end
)

sdm.method_add(
   drv, "free", "Free pin",
   function(bus, dev)
      local pin = sdm.attr_data(sdm.local_attr_handle(dev, "datapin"))
      if pin ~= nil then
         if pins[pin] == 1 then
            sdm.pin_free(pin)
            pins[pin] = nil
         else
            pins[pin] = pins[pin] - 1
         end
      end
   end
)

sdm.method_add(
   drv, "exchange", "Write and Read bytes",
   function(bus, dev, data, read_count)
      local pin = sdm.attr_data(sdm.local_attr_handle(dev, "datapin"))
      local id = sdm.attr_data(sdm.local_attr_handle(dev, "id"))
      if pin == nil or id == nil then return end
      ow.reset(pin)
      ow.select(pin, id)
      if data ~= nil then
         ow.write_bytes(pin, string.char(unpack(data)))
      end
      if read_count ~= nil and read_count > 0 then
         local rv = ow.read_bytes(pin, read_count)
         local rt = {}
         for i=1,#rv do
            rt[i] = rv:byte(i)
         end
         return rt
      end
   end
)

sdm.method_add(
   drv, "poll", "Poll for devices",
   function(bus, pin)
      local children = sdm.device_children(bus) or {}
      local ids = {}
      for name, handle in pairs(children) do
         local dpin = sdm.attr_data(sdm.local_attr_handle(handle, "pin"))
         if dpin == pin then
            ids[sdm.attr_data(sdm.local_attr_handle(handle, "id"))] = true
         end
      end
      ow.reset_search(pin)
      while true do
         local id = ow.search(pin)
         if id == nil then break end
         if ids[id] == nil then
            local name = ""
            for i=1,#id do name = name .. tostring(id:byte(i)) .. "_" end
            name = name:sub(1,-2)
            local device = sdm.device_add(name, bus)
            local rv = sdm.local_attr_add(device, "datapin", nil, pin, nil, nil)
            local rv = sdm.local_attr_add(device, "id", nil, id, nil, nil)
            local rv = sdm.device_poll(device)
         end
      end
   end
)

drv = nil
