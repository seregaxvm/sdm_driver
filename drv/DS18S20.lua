local drv = sdm.driver_add("DS18S20")

sdm.method_add(
   drv, "_poll", nil,
   function(dev, drv, par)
      local attr = sdm.attr_data(sdm.local_attr_handle(dev, "id"))
      if attr == nil then return false end
      return (sdm.device_name(par) == "ESP8266_1W") and (attr:byte(1) == 0x10)
   end
)

sdm.method_add(drv, "_init", nil,
               function(dev, drv, par)
                  sdm.device_rename(dev, sdm.request_name("DS18S20"))
                  sdm.attr_copy(dev, "temp")
                  local met = sdm.method_dev_handle(par, "setup")
                  local func = sdm.method_func(met)
                  func(par, dev)
               end
)

sdm.method_add(drv, "_free", nil,
               function(dev, drv, par)
                  local met = sdm.method_dev_handle(par, "free")
                  local func = sdm.method_func(met)
                  func(par, dev)
               end
)

sdm.method_add(drv, "measure", "Measure",
               function(dev)
                  local par = sdm.device_parent(dev)
                  local ex = sdm.method_func(sdm.method_dev_handle(par, "exchange"))
                  ex(par, dev, {0x44})
               end
)

sdm.attr_add(drv, "temp", "Temperature", "0",
             function(dev)
                local par = sdm.device_parent(dev)
                local attr = sdm.attr_dev_handle(dev, "temp")
                local ex = sdm.method_func(sdm.method_dev_handle(par, "exchange"))
                local temp = ex(par, dev, {0xbe}, 2)
                local whole = bit.rshift(temp[1], 1)
                local small = bit.band(temp[1], 1) * 5
                if temp[2] > 0 then
                   whole = whole * -1
                end
                temp = string.format("%d.%d", whole, small)
                sdm.attr_set(attr, temp)
                return temp
             end,
             nil
)

drv = nil
